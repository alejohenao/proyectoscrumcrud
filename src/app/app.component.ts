import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.styl']
})
export class AppComponent {
  title = 'frontend';
  
  constructor(private router:Router){}

  Listar(){
    this.router.navigate(["personas-list"]);
  }
  Crear(){
    this.router.navigate(["personas-create"])
  }
}

