import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IPersonas } from './personas/Personas';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class ServiceService {


  constructor(private http:HttpClient) { }  
//metodo para listar personas se comunica con el backend
  ListarPersonas():Observable <IPersonas[]>{
    return this.http.get<IPersonas[]>(`${environment.END_POINT}/api/personas`)
    .pipe(map(res => {
        return res;
      }));  
      }
//metodo para crear personas se comunica con el backend
   public CrearPersonas(persona: IPersonas):Observable<IPersonas>{
       return this.http.post<IPersonas>(`${environment.END_POINT}/api/personas`,persona)
       .pipe(map(res => {
        console.warn('persona',persona)
        return res;       
       }));
       };  
//metodo para actualizar personas se comunica con el backend
   ActualizarPersonas(personas: IPersonas):Observable<IPersonas>{
       return this.http.put<IPersonas>(`${environment.END_POINT}/api/personas`,personas)
        .pipe(map(res =>{
            return res;
        }));
   }
   //metodo para listar id personas se comunica con el backend
   ListarId(id: string):Observable<IPersonas>{
        return this.http.get<IPersonas>(`${environment.END_POINT}/api/personas/${id}`)
        .pipe(map(res =>{
            return res;
        }));
   }
   //metodo para eliminar personas se comunica con el backend
   EliminarPersonas(id: number):Observable<IPersonas>{
       return this.http.delete<IPersonas>(`${environment.END_POINT}/api/personas/${id}`)
        .pipe(map(res =>{
            return res;
        }));
   }
}