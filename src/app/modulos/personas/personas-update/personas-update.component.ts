import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder,FormGroup } from '@angular/forms';
import { ServiceService } from '../../personas.service';
import { IPersonas } from '../Personas';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-personas-update',
  templateUrl: './personas-update.component.html',
  styleUrls: ['./personas-update.component.styl']
})
export class PersonasUpdateComponent implements OnInit {
  formPersonasUpdate : FormGroup;
  constructor(
    private activatedRoute: ActivatedRoute,
    private service:ServiceService,
    private formBuilder: FormBuilder,
    private router: Router){ 

      this.formPersonasUpdate =this.formBuilder.group({
        id:[],
        documento:[''],
        nombre:[''],
        apellido:[''],
        direccion:[''],
        telefono:[''],
      });
     }

  ngOnInit(): void{
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    console.warn('ID',id );
    this.service.ListarId(id)
    .subscribe(res => {
      console.warn('mensaje de id',res);
      this.loadForm(res);
    }, res =>{});
       }
       //funcion para traer y carga los datos de personas
    private loadForm(personas:IPersonas ){
      this.formPersonasUpdate.patchValue({
        id:personas.id,
        documento:personas.documento,
        nombre: personas.nombre,
        apellido:personas.apellido,
        direccion:personas.direccion,
        telefono:personas.telefono
      });
    }
//metodo para actualizar
  Actualizar(): void{
    this.service.ActualizarPersonas(this.formPersonasUpdate.value)
    .subscribe(res =>{
      console.warn('actualizacion',res);
      this.router.navigate(['./personas/personas-list']);      
    }, err => {});
  }
  }


