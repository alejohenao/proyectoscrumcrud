import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonasDeleteComponent } from './personas-delete.component';

describe('PersonasDeleteComponent', () => {
  let component: PersonasDeleteComponent;
  let fixture: ComponentFixture<PersonasDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonasDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonasDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
