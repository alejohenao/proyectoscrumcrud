import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonasRoutingModule } from './personas-routing.module';
import { PersonasListComponent } from './personas-list/personas-list.component';
import { PersonasDeleteComponent } from './personas-delete/personas-delete.component';
import { PersonasCreateComponent } from './personas-create/personas-create.component';
import { PersonasUpdateComponent } from './personas-update/personas-update.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [PersonasListComponent, PersonasDeleteComponent, PersonasCreateComponent, PersonasUpdateComponent],
  imports: [
    CommonModule,
    PersonasRoutingModule,
    ReactiveFormsModule
  ],
  exports:[PersonasListComponent, PersonasDeleteComponent, PersonasCreateComponent, PersonasUpdateComponent]
})
export class PersonasModule { }
