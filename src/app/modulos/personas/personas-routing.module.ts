import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonasCreateComponent } from './personas-create/personas-create.component';
import { PersonasListComponent } from './personas-list/personas-list.component';
import { PersonasDeleteComponent } from './personas-delete/personas-delete.component';
import { PersonasUpdateComponent } from './personas-update/personas-update.component';


const routes: Routes = [
  {
    path:'personas-create',
    component:PersonasCreateComponent
  },
  {
    path:'personas-list',
    component:PersonasListComponent
  },
  {
    path:'personas-delete',
    component:PersonasDeleteComponent
  },
  {
    path:'personas-update',
    component:PersonasUpdateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonasRoutingModule { }
