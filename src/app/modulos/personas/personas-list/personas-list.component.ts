import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../../personas.service';
import { IPersonas } from '../Personas';
import { FormBuilder } from '@angular/forms';
@Component({
  selector: 'app-personas-list',
  templateUrl: './personas-list.component.html',
  styleUrls: ['./personas-list.component.styl']
})
export class PersonasListComponent implements OnInit {
 
listadoPersonas:IPersonas[];
  constructor(private service: ServiceService ) { }

//metodo para listar
  ngOnInit(): void {
    this.service.ListarPersonas().subscribe(res => {
      this.listadoPersonas=res;
    });
  }


// Eliminar(personas:IPersonas):void{
// console.log("eliminar");
//  this.service.EliminarPersonas(personas.id).subscribe(
//   res=>this.service.ListarPersonas().subscribe(
//    res=>this.listadoPersonas=res  
//    )      
//   );
//  }
 
Eliminar(personas:IPersonas):void{
  console.log("mensaje de eliminar",) 
 this.service.EliminarPersonas(personas.id).subscribe(() =>{  
  });
 }
}
