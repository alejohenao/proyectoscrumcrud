export interface IPersonas{
        id?:number;
        documento?:string;
        nombre?:string;
        apellido?:string;
        direccion?:string;
        telefono?:string;
}

export class Personas implements IPersonas{  
        constructor(
               public id?:number,
               public documento?:string,
               public nombre?:string,
               public apellido?:string,
               public direccion?:string,
               public telefono?:string
                ){}
            
}