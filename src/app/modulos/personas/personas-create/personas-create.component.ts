import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ServiceService } from '../../personas.service';
import { IPersonas } from '../Personas';

@Component({
  selector: 'app-personas-create',
  templateUrl: './personas-create.component.html',
  styleUrls: ['./personas-create.component.styl']
})
export class PersonasCreateComponent implements OnInit {


  personasForm = this.fb.group({
    documento:[''],
    nombre:[''],
    apellido:[''],
    direccion:[''],
    telefono:[''],
  })


  constructor(
    private fb: FormBuilder,
    private service:ServiceService) { }

  ngOnInit(): void {

  }
  //metodo para guardar datos 
  Guardar(): void {
    this.service.CrearPersonas(this.personasForm.value).subscribe(res => {
      console.warn('mensaje',res);
    });
    //resetear formulario
    this.personasForm.reset();
  }

}
