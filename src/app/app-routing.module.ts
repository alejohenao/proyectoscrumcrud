import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonasCreateComponent } from './modulos/personas/personas-create/personas-create.component';
import { PersonasListComponent } from './modulos/personas/personas-list/personas-list.component';
import { PersonasDeleteComponent } from './modulos/personas/personas-delete/personas-delete.component';
import { PersonasUpdateComponent } from './modulos/personas/personas-update/personas-update.component';


const routes: Routes = [
  {
    path:'personas',
    loadChildren: () => import ('./modulos/personas/personas.module')
    .then(m => m.PersonasModule)
  },{
    path:'personas-create',
    component:PersonasCreateComponent
  },
  {
    path:'personas-list',
    component:PersonasListComponent
  },
  {
    path:'personas-delete',
    component:PersonasDeleteComponent
  },
  {
    path:'personas-update',
    component:PersonasUpdateComponent
  }
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
